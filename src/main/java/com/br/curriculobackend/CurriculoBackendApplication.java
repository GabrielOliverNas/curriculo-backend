package com.br.curriculobackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CurriculoBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(CurriculoBackendApplication.class, args);
	}

}
